#!/bin/bash

set -e

cd "$(dirname "$0")" || exit 1

source variables.sh
source common.sh

usage() {
    stderr "Usage: $(basename "$0") -t GITLAB_TOKEN -p PROJECT_ID -n AGENT_NAME"
}

if [[ $# -lt 6 ]]; then
    usage
    exit 1
fi

while [[ $# -gt 0 ]]; do
    key="$1"
    case "$key" in
        -t|--gitlab-token)
            GITLAB_TOKEN="$2"
            shift
            shift
            ;;
        -p|--project-id)
            PROJECT_ID="$2"
            shift
            shift
            ;;
        -n|--agent-name)
            AGENT_NAME="$2"
            shift
            shift
            ;;
        *)
            stderr "Unknown argument: $2"
            exit 1
            ;;
    esac
done

ARGS_VALID=1

if [ -z "$GITLAB_TOKEN" ]; then
    stderr "Must provide gitlab token"
    ARGS_VALID=0
fi

if [ -z "$PROJECT_ID" ]; then
    stderr "Must provide project id"
    ARGS_VALID=0
fi

if [ -z "$AGENT_NAME" ]; then
    stderr "Must provide agent name"
    ARGS_VALID=0
fi

[ "$ARGS_VALID" = 1 ] || exit 1

stderr "Environment name: $CI_ENVIRONMENT_NAME"
stderr "Kubeconfig is: $KUBECONFIG"
stderr "Current context is: $(kubectl config current-context)"

agent-cli() {
    docker run --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable "$@"
}

PROJECT_PATH="$(request "$REST_URL/projects/$PROJECT_ID" | jq -r .path_with_namespace)"
echo "Project path is: $PROJECT_PATH"

setup_agent_graphql() {
    stderr "Creating new agent $AGENT_NAME in GitLab"
    create_agent="mutation createAgent {
      # agent-name should be the same as specified in the config.yaml
      createClusterAgent(input: { projectPath: \"$PROJECT_PATH\", name: \"$AGENT_NAME\" }) {
        clusterAgent {
          id
          name
        }
        errors
      }
    }"

    resp="$(request "$GRAPHQL_URL" -d "$(body "$create_agent")")" || exit 1
    id="$(echo "$resp" | jq -r .data.createClusterAgent.clusterAgent.id)"
    stderr "Agent ID: $(echo "$id" | tee "$AGENT_ID_FILE")"
    token_name="$AGENT_NAME-token"

    stderr 'Creating new agent token'
    create_agent_token="mutation createToken {
      clusterAgentTokenCreate(
        input: {
          clusterAgentId: \"$id\"
          name: \"$token_name\"
        }
      ) {
        secret
        token {
          createdAt
          id
        }
        errors
      }
    }"

    resp="$(request "$GRAPHQL_URL" -d "$(body "$create_agent_token")")" || exit 1
    token_id="$(echo "$resp" | jq -r .data.clusterAgentTokenCreate.token.id)"
    stderr "Token ID: $token_id"
    echo "$resp" | jq -r .data.clusterAgentTokenCreate.secret
}

install_gitlab_agent() {
    AGENT_TOKEN="$1"
    stderr 'Creating namespace for gitlab agent'
    kubectl get namespaces
    kubectl get namespaces | grep gitlab-kubernetes-agent || kubectl create namespace gitlab-kubernetes-agent
    stderr 'Installing GitLab Agent'
    agent-cli generate \
              --agent-token="$AGENT_TOKEN" \
              --kas-address=wss://kas."$GITLAB_HOST" \
              --agent-version stable \
              --namespace gitlab-kubernetes-agent | kubectl apply -f -
    kubectl wait --for=condition=available --timeout=100s deployment/gitlab-agent -n gitlab-kubernetes-agent
}

apply_helmfile() {
    stderr 'Applying helmfile.yaml'
    helmfile --file helmfile.yaml apply
}

apply_nginx_deployment() {
    stderr 'Creating vulnerable NGINX deployment'
    kubectl apply -f deployment.yaml
    kubectl wait --for=condition=available --timeout=100s deployment/vulnerable-nginx
}

secret="$(setup_agent_graphql)"
install_gitlab_agent "$secret"
apply_helmfile
apply_nginx_deployment

stderr "Sleeping for 30 seconds for Starboard to transmit vulnerability report"
sleep 30
agent_pod="$(kubectl get pod --no-headers -n gitlab-kubernetes-agent | head -n 1 | cut -d ' ' -f1)"
kubectl logs "$agent_pod" -n gitlab-kubernetes-agent

kubectl get vulnerabilityreports
kubectl get -o yaml vulnerabilityreport -l starboard.container.name=nginx

jsonpath="{range .items[*]}{.metadata.name} {.status.phase}{'\n'}{end}"
kubectl get pod \
        -n starboard-system \
        -l app.kubernetes.io/name=starboard-operator \
        -o jsonpath="$jsonpath"
kubectl get pod \
        -n gitlab-kubernetes-agent \
        -l app=gitlab-agent \
        -o jsonpath="$jsonpath"
