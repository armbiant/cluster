# demo-cluster

* Registers a new Agent token to the GitLab project
* Installs the Agent in the cluster
* Creates a starboard-operator Helm release
* Creates a vulnerable NGINX Deployment

## Usage

If all you need is a project with Cluster Image Scanning vulnerabilities:

1. Fork this project
2. [Set the `GITLAB_TOKEN` CI/CD variable](https://docs.gitlab.com/ee/ci/examples/semantic-release.html#set-up-cicd-variables)
3. Create a pipeline for the master branch

CI pipelines create ephemeral clusters from which an ad-hoc registered Agent sends a vulnerability reports.

Otherwise, provision an existing cluster, e.g. GKE:

1. Switch to the kubectl context of the cluster

2. Run a demo-cluster container locally with mounted kubeconfig:

```sh
docker run \
	-v /var/run/docker.sock:/var/run/docker.sock \
	-v $HOME/.kube/config:/var/run/kubeconfig \
	registry.gitlab.com/gitlab-org/protect/demos/demo-cluster/master \
	-p PROJECT_ID -t GITLAB_TOKEN -n AGENT_NAME
```

where:

* `PROJECT_ID` is the numeric ID of a GitLab project
* `GITLAB_TOKEN` is a GitLab access token for the project
* `AGENT_NAME` is the name of the registered Agent as referenced by its configuration

## Vulnerable NGINX deployment

demo-cluster creates a Deployment of the vulnerable `nginx:1` image.

To enable the Agent's Starboard module, add an agent configuration file to the project for `AGENT_NAME`:

```yaml
# .gitlab/agents/AGENT_NAME/config.yaml
starboard:
  vulnerability_report:
    namespaces:
      - default
```

The vulnerable NGINX Deployment can be deleted with:

```sh
kubectl delete deployment/vulnerable-nginx
```

## Configuration

The following environment variables are respected:

| Name        | Default    |
|-------------|------------|
| GITLAB_HOST | gitlab.com |
| PROTO       | https      |
| PORT        | 443        |
